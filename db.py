import psycopg2


def get_collection_conn(namespace):
    if namespace == 'localhost':
        return psycopg2.connect(dbname="eltezam-collection", host="localhost",
                                user="postgres", password="postgres", port="5435")

    return psycopg2.connect(dbname="eltezam-collection", host="postgresql."+namespace+".svc.cluster.local", user="postgres", password="postgres", port="5432")


def get_backend_conn(namespace):
    if namespace == 'localhost':
        return psycopg2.connect(dbname="eltezam-collection", host="localhost",
                                user="postgres", password="postgres", port="5435")

    return psycopg2.connect(dbname="eltezam-backend", host="postgresql."+namespace+".svc.cluster.local", user="postgres", password="postgres", port="5432")