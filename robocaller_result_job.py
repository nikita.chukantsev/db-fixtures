from cleaner import Cleaner
from entities import debtor, debtor_campaigns, debtor_contact, loan_call_queue, loan, agents, buckets, robocaller_history, company
from db import get_collection_conn, get_backend_conn

# TODO change variables
review_namespace = "fix-robocaller-result-jo"  # todo k8s namespace
campaign_template_id = "temp_auto-info_1m_young_all_v0"  # todo change robocoller template id
debtor_phone = "+971508997475"  # todo change debtor phone
call_id = "bc832e50-9a4a-547c-05dd-b62fc653331e"  # todo replace with real ziwo call id

agent_number = "+971521538598"
debtor_name = "Full Name"
principal, interest, fees, paid = 1000, 0, 0, 0

collectionConn = get_collection_conn(review_namespace)
backendConn = get_backend_conn(review_namespace)

cleaner = Cleaner([
    robocaller_history.RobocallerHistoryCleaner(collectionConn),
    debtor_campaigns.DebtorCampaignsCleaner(collectionConn),
    loan_call_queue.LoanCallQueueCleaner(collectionConn),
    loan.LoansCleaner(backendConn),
    debtor_contact.DebtorContactsCleaner(backendConn),
    debtor.DebtorsCleaner(backendConn),
    agents.AgentsCleaner(collectionConn),
    buckets.BucketsCleaner(collectionConn),
    company.CompaniesCleaner(backendConn)
]).clear()

company = company.Company(bucket='fake-bucket')
company.create(backendConn)

bucket = buckets.Bucket(id=1, country='SAU')
bucket.create(collectionConn)
agent = agents.Agent(number=agent_number, bucket_id=bucket.id, is_robot=True)
agent.create(collectionConn)

debtor = debtor.Debtor(company_id=company.id, full_name=debtor_name, language=debtor.LANG_ENG)
debtor.create(backendConn)

debtorPhone = debtor_contact.DebtorContact(debtor_id=debtor.id, item_type=debtor_contact.ITEM_TYPE_PHONE, value=debtor_phone)
debtorPhone.create(backendConn)

loan = loan.Loan(company_id=company.id, debtor_id=debtor.id, status=loan.STATUS_OVERDUE, principal=principal, interest=interest, fees=fees, paid=paid)
loan.create(backendConn)

loan_call_queue = loan_call_queue.LoanCallQueue(
    customer_id=debtor.id,
    loan_id=loan.id,
    collection_status=loan_call_queue.COLLECTION_STATUS_NEW,
)
loan_call_queue.create(collectionConn)

debtor_campaign = debtor_campaigns.DebtorCampaigns(
    customer_id=debtor.id,
    template_id=campaign_template_id,
    channel=debtor_campaigns.CHANNEL_AUTO_INFORMER,
    status=debtor_campaigns.STATUS_IN_PROGRESS,
)
debtor_campaign.create(collectionConn)

history = robocaller_history.RobocallerHistory(
    call_id=call_id,
    customer_id=debtor.id,
    agent_id=agent.id,
    loan_ids=[str(loan.id)],
    status=None,
    processed=False,
    campaign_id=debtor_campaign.id,
)
history.create(collectionConn)


collectionConn.close()
backendConn.close()
