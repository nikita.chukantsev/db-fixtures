class Bucket:
    def __init__(self, id, country):
        self.id = id
        self.country = country
        self.start_of_range = 0
        self.end_of_range = 2
        self.num = 0
        self.delinquency_days = '{10,15,30,40}'

    def create(self, conn):
        cursor = conn.cursor()

        cursor.execute(
            "INSERT INTO buckets(id, country, start_of_range, end_of_range, num, delinquency_days) VALUES(%s, %s, %s, %s, %s, %s) ON CONFLICT DO NOTHING",
            (self.id, self.country, self.start_of_range, self.end_of_range, self.num, self.delinquency_days)
        )
        conn.commit()

        cursor.close()


class BucketsCleaner:
    def __init__(self, conn):
        self.conn = conn

    def clear(self):
        cursor = self.conn.cursor()
        cursor.execute("DELETE FROM buckets")
        self.conn.commit()
        cursor.close()
