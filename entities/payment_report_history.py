import uuid
from datetime import datetime, timezone, timedelta


class PaymentReportHistoryCleaner:
    def __init__(self, conn):
        self.conn = conn

    def clear(self):
        cursor = self.conn.cursor()
        cursor.execute("DELETE FROM payment_report_history")
        self.conn.commit()
        cursor.close()
