import uuid

CHANNEL_SMS = "sms"
CHANNEL_AUTO_INFORMER = "auto_informer"
CHANNEL_WHATS_APP = "whatsapp"
CHANNEL_CALL = "call"
CHANNEL_EMAIL = "email"
CHANNEL_DCA = "dca"

STATUS_PENDING = "pending"
STATUS_ERROR = "error"
STATUS_IN_PROGRESS = "in_progress"


class DebtorCampaigns:
    def __init__(self, customer_id, channel, template_id, status):
        self.id = uuid.uuid4()
        self.customer_id = customer_id
        self.channel = channel
        self.template_id = template_id
        self.status = status
        self.country = 'SAU'
        self.score_id = uuid.uuid4()
        self.notify_after = 1

    def create(self, conn):
        cursor = conn.cursor()

        cursor.execute(
            "INSERT INTO debtor_campaigns(id, customer_id, channel, template_id, status, country, score_id, notify_after, priority, attempts_left, created_at, updated_at) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, NOW(), NOW())",
            (str(self.id), str(self.customer_id), str(self.channel), self.template_id, self.status, self.country, str(self.score_id), self.notify_after, 0, 3)
        )
        conn.commit()

        cursor.close()


class DebtorCampaignsCleaner:
    def __init__(self, conn):
        self.conn = conn

    def clear(self):
        cursor = self.conn.cursor()
        cursor.execute("DELETE FROM debtor_campaigns")
        self.conn.commit()
        cursor.close()
