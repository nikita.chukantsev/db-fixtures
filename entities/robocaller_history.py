from datetime import datetime, timezone, timedelta


class RobocallerHistory:
    def __init__(self, call_id, customer_id, agent_id, loan_ids, status, processed, campaign_id):
        self.call_id = call_id
        self.customer_id = customer_id
        self.agent_id = agent_id
        self.loan_ids = loan_ids
        self.status = status
        self.processed = processed
        self.updated_at = datetime.now(timezone.utc) - timedelta(hours=9)
        self.created_at = datetime.now(timezone.utc) - timedelta(hours=9)
        self.payload = '{}'
        self.campaign_id = campaign_id

    def create(self, conn):
        cursor = conn.cursor()

        cursor.execute(
            "INSERT INTO robocaller_history(call_id, customer_id, agent_id, loan_ids, status, processed, updated_at, created_at, payload, campaign_id) VALUES(%s, %s, %s, %s::uuid[], %s, %s, %s, %s, %s, %s)",
            (str(self.call_id), str(self.customer_id), str(self.agent_id), self.loan_ids, self.status, self.processed,
             self.updated_at, self.created_at, self.payload, str(self.campaign_id))
        )
        conn.commit()

        cursor.close()


class RobocallerHistoryCleaner:
    def __init__(self, conn):
        self.conn = conn

    def clear(self):
        cursor = self.conn.cursor()
        cursor.execute("DELETE FROM robocaller_history")
        self.conn.commit()
        cursor.close()
