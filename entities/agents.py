import uuid


class Agent:
    def __init__(self, number, bucket_id, is_robot):
        self.id = uuid.uuid4()
        self.bucket_id = bucket_id
        self.number = number
        self.is_robot = is_robot
        self.enabled = True

    def create(self, conn):
        cursor = conn.cursor()

        cursor.execute(
            "INSERT INTO agents(id, bucket_id, number, is_robot, enabled) VALUES(%s, %s, %s, %s, %s)",
            (str(self.id), self.bucket_id, self.number, self.is_robot, self.enabled)
        )
        conn.commit()

        cursor.close()


class AgentsCleaner:
    def __init__(self, conn):
        self.conn = conn

    def clear(self):
        cursor = self.conn.cursor()
        cursor.execute("DELETE FROM agents")
        self.conn.commit()
        cursor.close()
