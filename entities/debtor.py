import uuid

LANG_ENG = "eng"


class Debtor:
    def __init__(self, company_id, full_name, language):
        self.id = uuid.uuid4()
        self.company_id = company_id
        self.full_name = full_name
        self.language = language

    def create(self, conn):
        cursor = conn.cursor()

        cursor.execute(
            "INSERT INTO debtors(id, reference_id, company_id, debtor_eltezam_id, full_name, language, created_at, updated_at) VALUES(%s, %s, %s, %s, %s, %s, NOW(), NOW())",
            (str(self.id), str(uuid.uuid4()), str(self.company_id), str(uuid.uuid4()), self.full_name, self.language)
        )
        conn.commit()

        cursor.close()


class DebtorsCleaner:
    def __init__(self, conn):
        self.conn = conn

    def clear(self):
        cursor = self.conn.cursor()
        cursor.execute("DELETE FROM debtors")
        self.conn.commit()
        cursor.close()
