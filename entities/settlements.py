import uuid
from datetime import datetime, timezone, timedelta


class Settlements:
    def __init__(self, company_id, loan_reference_id, debtor_reference_id, repayment_id, paid_amount, currency):
        self.id = uuid.uuid4()
        self.company_id = company_id
        self.loan_reference_id = loan_reference_id
        self.debtor_reference_id = debtor_reference_id
        self.repayment_id = repayment_id
        self.paid_amount = paid_amount
        self.currency = currency
        self.date = datetime.now(timezone.utc) - timedelta(days=1)

    def create(self, conn):
        cursor = conn.cursor()

        cursor.execute(
            "INSERT INTO settlements(id, company_id, loan_reference_id, debtor_reference_id, repayment_id, paid_amount, currency, date) VALUES(%s, %s, %s, %s, %s, %s, %s, %s)",
            (str(self.id), str(self.company_id), str(self.loan_reference_id), str(self.debtor_reference_id), str(self.repayment_id), self.paid_amount, self.currency, self.date)
        )
        conn.commit()

        cursor.close()


class SettlementsCleaner:
    def __init__(self, conn):
        self.conn = conn

    def clear(self):
        cursor = self.conn.cursor()
        cursor.execute("DELETE FROM settlements")
        self.conn.commit()
        cursor.close()
