import uuid

ITEM_TYPE_EMAIL = "EMAIL"
ITEM_TYPE_PHONE = "PHONE"

CONTACT_TYPE_WORK = "WORK"


class DebtorContact:
    def __init__(self, debtor_id, item_type, value):
        self.id = uuid.uuid4()
        self.debtor_id = debtor_id
        self.item_type = item_type
        self.contact_type = CONTACT_TYPE_WORK
        self.value = value
        self.is_primary = True

    def create(self, conn):
        cursor = conn.cursor()

        cursor.execute(
            "INSERT INTO debtor_contacts(id, debtor_id, type, contact_type, is_primary, value) VALUES(%s, %s, %s, %s, %s, %s)",
            (str(self.id), str(self.debtor_id), self.item_type, self.contact_type, self.is_primary, self.value)
        )
        conn.commit()

        cursor.close()


class DebtorContactsCleaner:
    def __init__(self, conn):
        self.conn = conn

    def clear(self):
        cursor = self.conn.cursor()
        cursor.execute("DELETE FROM debtor_contacts")
        self.conn.commit()
        cursor.close()
