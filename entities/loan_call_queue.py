import uuid

COLLECTION_STATUS_NEW = "NEW"
COLLECTION_STATUS_IN_COLLECTION = "IN_COLLECTION"
COLLECTION_STATUS_SUCCESS = "SUCCESS"

TYPE_LOAN = 'loan'
TYPE_PP = 'payment_plan'

class LoanCallQueue:
    def __init__(self, customer_id, loan_id, collection_status, debt_type=TYPE_LOAN):
        self.customer_id = customer_id
        self.loan_id = loan_id
        self.type = debt_type
        self.country = 'SAU'
        self.call_attempts = 0
        self.collection_status = collection_status

    def create(self, conn):
        cursor = conn.cursor()

        cursor.execute(
            "INSERT INTO loan_calls_queue(customer_id, loan_id, type, country, call_attempts, collection_status, overdue_start_date, created_at, updated_at ,scored_at) VALUES(%s, %s, %s, %s, %s, %s, NOW(), NOW(), NOW(), NOW())",
            (str(self.customer_id), str(self.loan_id), self.type, self.country, self.call_attempts, self.collection_status)
        )
        conn.commit()

        cursor.close()


class LoanCallQueueCleaner:
    def __init__(self, conn):
        self.conn = conn

    def clear(self):
        cursor = self.conn.cursor()
        cursor.execute("DELETE FROM loan_calls_queue")
        self.conn.commit()
        cursor.close()
