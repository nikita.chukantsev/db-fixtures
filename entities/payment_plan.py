import json
import uuid
from datetime import datetime, timezone

STATUS_ACTIVE = "active"
STATUS_OVERDUE = "overdue"
STATUS_COMPLETED = "completed"
STATUS_CANCELED = "canceled"


class PaymentPlan:
    def __init__(self, debtor_id, loan_ids, status):
        self.id = uuid.uuid4()
        self.debtor_id = debtor_id
        self.loan_ids = loan_ids
        self.status = status
        self.created_at = datetime.now(timezone.utc)
        self.updated_at = datetime.now(timezone.utc)

    def create(self, conn):
        cursor = conn.cursor()

        cursor.execute(
            "INSERT INTO payment_plans(id, debtor_id, loan_ids, status, billing_loan_id, notify_channel, created_at, updated_at) VALUES(%s, %s, %s, %s, %s, %s, %s, %s)",
            (str(self.id), str(self.debtor_id), self.loan_ids, self.status, "", "email", self.created_at, self.updated_at)
        )
        conn.commit()

        cursor.close()


class PaymentPlanCleaner:
    def __init__(self, conn):
        self.conn = conn

    def clear(self):
        cursor = self.conn.cursor()
        cursor.execute("DELETE FROM payment_plans")
        self.conn.commit()
        cursor.close()
