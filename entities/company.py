import uuid
from datetime import datetime, timezone


class Company:
    def __init__(self, bucket: str):
        self.id = uuid.uuid4()
        self.type = 'BANK'
        self.name = 'Company name'
        self.bucket = bucket
        self.phone = '+971520000000'
        self.app_url = 'http://app.url'
        self.created_at = datetime.now(timezone.utc)
        self.updated_at = datetime.now(timezone.utc)

    def create(self, conn):
        cursor = conn.cursor()

        cursor.execute(
            "INSERT INTO companies(id, type, name, bucket, phone, app_url, created_at, updated_at) VALUES(%s, %s, %s, %s, %s, %s, %s, %s)",
            (str(self.id), self.type, self.name, self.bucket, self.phone, self.app_url, self.created_at, self.updated_at)
        )
        conn.commit()

        cursor.close()


class CompaniesCleaner:
    def __init__(self, conn):
        self.conn = conn

    def clear(self):
        cursor = self.conn.cursor()
        cursor.execute("DELETE FROM companies")
        self.conn.commit()
        cursor.close()
