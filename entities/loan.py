import uuid
from datetime import datetime, timezone

STATUS_ACTIVE = "active"
STATUS_OVERDUE = "overdue"
STATUS_COMPLETED = "completed"
STATUS_STOPPED = "stopped"
STATUS_REFUNDED = "refunded"


class Loan:
    def __init__(self, company_id, debtor_id, status, principal, interest, fees, paid):
        self.id = uuid.uuid4()
        self.reference_id = uuid.uuid4()
        self.company_id = company_id
        self.debtor_id = debtor_id
        self.currency = "AED"
        self.principal = principal
        self.interest = interest
        self.fees = fees
        self.paid = paid
        self.status = status
        self.debt_issued = datetime.now(timezone.utc)
        self.due_date = datetime.now(timezone.utc)

    def create(self, conn):
        cursor = conn.cursor()

        cursor.execute(
            "INSERT INTO loans(id, reference_id, company_id, debtor_id, debt_issued, due_date, currency, paid_before_transfer, principal, interest, fees, paid, status, product_type, created_at, updated_at) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, NOW(), NOW())",
            (str(self.id), str(self.reference_id), str(self.company_id), str(self.debtor_id), self.debt_issued, self.due_date, self.currency, 0, self.principal, self.interest, self.fees, self.paid, self.status, "product type")
        )
        conn.commit()

        cursor.close()


class LoansCleaner:
    def __init__(self, conn):
        self.conn = conn

    def clear(self):
        cursor = self.conn.cursor()
        cursor.execute("DELETE FROM loans")
        self.conn.commit()
        cursor.close()
