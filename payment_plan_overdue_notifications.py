from cleaner import Cleaner
from entities import debtor, debtor_campaigns, debtor_contact, loan_call_queue, loan, agents, buckets, robocaller_history, company, payment_plan
from db import get_collection_conn, get_backend_conn

# TODO change variables
review_namespace = "elt-590-support-pp-templ"  # todo k8s namespace
campaign_template_id = "temp_email_3f_pp_normal_all_v0"  # todo change template id
debtor_email = "nikita.chukantsev@tabby.ai"  # todo change debtor email

agent_number = "+971521538598"
debtor_name = "Full Name"
principal, interest, fees, paid = 1000, 0, 0, 0

collectionConn = get_collection_conn(review_namespace)
backendConn = get_backend_conn(review_namespace)

cleaner = Cleaner([
    robocaller_history.RobocallerHistoryCleaner(collectionConn),
    debtor_campaigns.DebtorCampaignsCleaner(collectionConn),
    loan_call_queue.LoanCallQueueCleaner(collectionConn),
    payment_plan.PaymentPlanCleaner(backendConn),
    loan.LoansCleaner(backendConn),
    debtor_contact.DebtorContactsCleaner(backendConn),
    debtor.DebtorsCleaner(backendConn),
    company.CompaniesCleaner(backendConn)
]).clear()

company = company.Company(bucket='fake-bucket')
company.create(backendConn)

debtor = debtor.Debtor(company_id=company.id, full_name=debtor_name, language=debtor.LANG_ENG)
debtor.create(backendConn)

debtorEmail = debtor_contact.DebtorContact(debtor_id=debtor.id, item_type=debtor_contact.ITEM_TYPE_EMAIL, value=debtor_email)
debtorEmail.create(backendConn)

loan = loan.Loan(company_id=company.id, debtor_id=debtor.id, status=loan.STATUS_ACTIVE, principal=principal, interest=interest, fees=fees, paid=paid)
loan.create(backendConn)

pp = payment_plan.PaymentPlan(debtor_id=debtor.id, loan_ids=[str(loan.id)], status=payment_plan.STATUS_OVERDUE)
pp.create(backendConn)

loan_call_queue = loan_call_queue.LoanCallQueue(
    customer_id=debtor.id,
    loan_id=pp.id,
    debt_type=loan_call_queue.TYPE_PP,
    collection_status=loan_call_queue.COLLECTION_STATUS_NEW,
)
loan_call_queue.create(collectionConn)

debtor_campaign = debtor_campaigns.DebtorCampaigns(
    customer_id=debtor.id,
    template_id=campaign_template_id,
    channel=debtor_campaigns.CHANNEL_EMAIL,
    status=debtor_campaigns.STATUS_PENDING,
)
debtor_campaign.create(collectionConn)

collectionConn.close()
backendConn.close()
