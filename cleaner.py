class Cleaner:
    def __init__(self, entities: list):
        self.entities = entities

    def clear(self):
        for entity in self.entities:
            entity.clear()
