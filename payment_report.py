import uuid
from cleaner import Cleaner
from db import get_collection_conn, get_backend_conn
from entities import settlements, company, payment_report_history

# TODO change variables
review_namespace = "fix-payment-report-metri"  # todo k8s namespace

collectionConn = get_collection_conn(review_namespace)
backendConn = get_backend_conn(review_namespace)

cleaner = Cleaner([
    payment_report_history.PaymentReportHistoryCleaner(backendConn),
    settlements.SettlementsCleaner(backendConn),
    company.CompaniesCleaner(backendConn),
]).clear()

company = company.Company(bucket='fake-bucket')
company.create(backendConn)

settlements.Settlements(
    company_id=company.id,
    loan_reference_id=uuid.uuid4(),
    debtor_reference_id=uuid.uuid4(),
    repayment_id=uuid.uuid4(),
    paid_amount=1000,
    currency='AED',
).create(backendConn)
settlements.Settlements(
    company_id=company.id,
    loan_reference_id=uuid.uuid4(),
    debtor_reference_id=uuid.uuid4(),
    repayment_id=uuid.uuid4(),
    paid_amount=2000,
    currency='AED',
).create(backendConn)

collectionConn.close()
backendConn.close()
